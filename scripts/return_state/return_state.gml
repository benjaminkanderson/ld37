///@description move_to
///@param argument_array
var _state_arguments = argument0;

var _xprevious = _state_arguments[0];
var _yprevious = _state_arguments[1];
var _is_player = _state_arguments[2];

if (!move(_xprevious, _yprevious, SPEED)) {
	if (_is_player) {
		// Wait
		if (energy < max_energy) {
			energy = min(++energy, max_energy);
			var _text = instance_create_layer(x+CELL_SIZE/2, y-CELL_SIZE/2, "Instances", o_text);
			_text.image_blend = make_color_hsv(40, 150, 255);
			_text.text = "+"+string(1);
		}
	}
	event_user(END_TURN);
}
///@description move_to
///@param argument_array
var _state_arguments = argument0;

// Exit case
var _xprevious = _state_arguments[0];
var _yprevious = _state_arguments[1];
var _xnext = _state_arguments[2];
var _ynext = _state_arguments[3];
var _is_player = _state_arguments[4];

// Update the yoffset
if (!move(_xnext, _ynext, SPEED)) {
	yoffset = 0;
	if (object_index == o_bat) {
		if (moves > 0) {
			moves--;
			change_states(action_state, [false]);
		} else {
			moves = 1;
			event_user(END_TURN);
		}
	} else {
		event_user(END_TURN);
	}
	ds_grid_set(global.unit_grid, _xprevious div CELL_SIZE, _yprevious div CELL_SIZE, noone);
	ds_grid_set(global.unit_grid, x div CELL_SIZE, y div CELL_SIZE, id);
	audio_play_sound(a_land, 5, false);
} else {
	var _total_distance = point_distance(_xprevious, _yprevious, _xnext, _ynext);
	var _distance = point_distance(_xprevious, _yprevious, x, y,);
	yoffset = abs(sin(pi/(_total_distance/_distance)))*JUMP_HEIGHT;
	image_xscale = (x == _xprevious) ? image_xscale : sign(_xnext-x);
}
if (textbox.text_visible != textbox.text) {
	textbox.text_visible = textbox.text;
} else {
	instance_destroy(textbox);
	instance_destroy();
	
	if (instance_exists(o_cutscene)) {
		o_cutscene.action++;
	} else {
		activate_next_unit();
	}
}
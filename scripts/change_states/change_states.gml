///@description change_states
///@param state
///@param argument_array
var _state = argument0;
var _argument_array = argument1;

state = _state;
state_arguments = _argument_array;
if (mouse_check_button_pressed(mb_left)) {
	// Try to attack
	var _target_gx = snap_value(mouse_x, CELL_SIZE)/CELL_SIZE;
	var _target_gy = snap_value(mouse_y, CELL_SIZE)/CELL_SIZE;
	var enemy =	global.unit_grid[# _target_gx, -_target_gy];
	var _dis = point_distance(snap_value(x, CELL_SIZE)/CELL_SIZE, snap_value(y, CELL_SIZE)/CELL_SIZE, _target_gx, _target_gy);
	if (enemy && _dis < 4) {
		if (enemy != id) {
			enemy.hp -= att;
			set_shake(enemy, 2, seconds_to_steps(.25));
			set_shake(o_camera, 2, seconds_to_steps(.2));
			create_blood(enemy.x+CELL_SIZE/2, enemy.y+CELL_SIZE/2, 4, 2);
			energy -= 2;
		}
	}
}
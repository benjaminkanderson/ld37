///@param x
///@param y
///@param text
///@param action

var _x = argument0;
var _y = argument1;
var _text = argument2;
var _action = argument3;

var _button = instance_create_layer(_x, _y, "Instances", o_ui_button);
with (_button) {
	text = _text;
	action = _action;
	width = string_width(_text)+margin;
	height = string_height(_text)+margin;
	
	// Update the scale
	image_xscale = width/sprite_get_width(sprite_index);
	image_yscale = height/sprite_get_height(sprite_index);
}

return _button;
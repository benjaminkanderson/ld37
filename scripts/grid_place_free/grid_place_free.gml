/// @Description
/// @param x
/// @param y
var _x = argument0;
var _y = argument1;

var _grid_x = floor(_x/CELL_SIZE);
var _grid_y = floor(_y/CELL_SIZE);

var _layer_id = layer_get_id("Tiles");
var _map_id = layer_tilemap_get_id(_layer_id);
var _tile_id =  tilemap_get_at_pixel(_map_id, _x, _y);

var _tile_free = !is_in_array(_tile_id, [VOID, EMPTY, EDGE]);
var _object_free = global.unit_grid[# _grid_x, _grid_y] == noone;

return _tile_free && _object_free;
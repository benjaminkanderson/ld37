///@param x
///@param y
///@param amount
///@param offset_amount
var _x = argument0;
var _y = argument1;
var _amount = argument2;
var _offset_amount = argument3;

repeat (_amount) {
	var _xoff = irandom_range(-_offset_amount, _offset_amount);
	var _yoff = irandom_range(-_offset_amount, _offset_amount);
	instance_create_layer(_x+_xoff, _y+_yoff, "Instances", o_blood);
}
with (global.unit_list[| global.unit_index]) {
	var _is_player = object_index == o_wizard;
	change_states(action_state, [_is_player]);
}
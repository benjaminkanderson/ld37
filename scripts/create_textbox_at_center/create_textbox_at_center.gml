///@param text
var _text = argument0;
var _textbox = create_textbox(4, 4, 8, argument0);
_textbox.ui_x = camera_get_view_width(view_camera[0])/2 - _textbox.width/2;
_textbox.ui_y = camera_get_view_height(view_camera[0])/2 - _textbox.height/2;

return _textbox;
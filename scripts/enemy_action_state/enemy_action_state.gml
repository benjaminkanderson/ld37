///@description enemy_action_state
///@param state_arguments
var _state_arguments = argument0;

// Exit case
if (!instance_exists(o_wizard)) exit;

// Move towards the player
var _dir = snap_value(point_direction(x, y, o_wizard.x, o_wizard.y), 90);
if (grid_direction_free(x, y, _dir)) {
	var _xnext = x+lengthdir_x(CELL_SIZE, _dir);
	var _ynext = y+lengthdir_y(CELL_SIZE, _dir);
	//audio_play_sound(a_jump, 5, false);
	change_states(move_to_state, [x, y, _xnext, _ynext, false]);
	image_xscale = (o_wizard.x - x != 0) ? sign(o_wizard.x - x) : image_xscale;
	exit;
} if (point_distance(x, y, o_wizard.x, o_wizard.y) == CELL_SIZE) {
	var _xnext = x+lengthdir_x(CELL_SIZE, _dir);
	var _ynext = y+lengthdir_y(CELL_SIZE, _dir);
	var enemy =	global.unit_grid[# _xnext/CELL_SIZE, _ynext/CELL_SIZE];
	if (enemy) {
		change_states(attack_state, [x, y, _xnext, _ynext, false]);
		if (instance_exists(o_action_cue)) {
			instance_destroy(o_action_cue);
		}
		exit;
	}
}

// Try to move randomly
var new_locations = ds_list_create();
for (_dir=0; _dir<360; _dir+=90) {
	if (grid_direction_free(x, y, _dir)) {
		ds_list_add(new_locations, _dir);
	}
}

// Select from possible directions
var _list_size = ds_list_size(new_locations);
if (_list_size > 0) {
	// Move
	_dir = new_locations[| irandom(_list_size-1)];
	var _xnext = x+lengthdir_x(CELL_SIZE, _dir);
	var _ynext = y+lengthdir_y(CELL_SIZE, _dir);
	//audio_play_sound(a_jump, 5, false);
	change_states(move_to_state, [x, y, _xnext, _ynext, false]);
} else {
	// End turn
	event_user(END_TURN);
}

// Destroy the list
ds_list_destroy(new_locations);
///@description action_state
///@param argument_array
var _state_arguments = argument0;

// Exit case
if (array_length_1d(_state_arguments) != 1) exit;
var _is_player = _state_arguments[0];

// Choose the correct state
if (_is_player) {
	change_states(player_action_state, []);
} else {
	change_states(enemy_action_state, []);
}
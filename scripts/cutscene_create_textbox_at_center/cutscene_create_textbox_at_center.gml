///@param text
///@param action
var _text = argument0;
var _action = argument1;

if (!instance_exists(o_ui_textbox)) {
	var _textbox = create_textbox_at_center(_text);
	_textbox.button_action = _action;
	
	with (_textbox) {
		if (button == noone && button_action != noone) {
			button = create_button(142, 70, ">", destroy_textbox_action);
			button.textbox = id;
			button.action = button_action;
		}
	}
}
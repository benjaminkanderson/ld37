///@description action_state
///@param argument_array
var _state_arguments = argument0;

if (!instance_exists(o_action_cue)) {
	instance_create_layer(x, y, "Instances", o_action_cue);
}


// Get the action
if (mouse_check_button_pressed(mb_left)) {
	var _xnext = snap_value(mouse_x, CELL_SIZE);
	var _ynext = snap_value(mouse_y, CELL_SIZE);

	// Take Action
	var _dis = point_distance(x, y, _xnext, _ynext);
	if (grid_place_free(_xnext, _ynext) && _dis == CELL_SIZE) {
		// Try to Move
		change_states(move_to_state, [x, y, _xnext, _ynext, true]);
		//audio_play_sound(a_jump, 5, false);
		if (instance_exists(o_action_cue)) {
			instance_destroy(o_action_cue);
		}
	} else {
		// Try to attack
		var enemy =	global.unit_grid[# _xnext/CELL_SIZE, _ynext/CELL_SIZE];
		if (enemy && _dis < CELL_SIZE*2) {
			if (enemy != id) {
				// Attack
				change_states(attack_state, [x, y, _xnext, _ynext, true]);
				if (instance_exists(o_action_cue)) {
					instance_destroy(o_action_cue);
				}
			} else {
				// Wait
				event_user(END_TURN);
				if (instance_exists(o_action_cue)) {
					instance_destroy(o_action_cue);
				}
			}
		}
	}
}
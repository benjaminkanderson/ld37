/// @description Initialize some game information
// Cell size
#macro CELL_SIZE	16

// Tile IDs
#macro VOID		-1
#macro EMPTY	0
#macro EDGE		5

// Font string
#macro FONT_STRING "ABCDEFGHIJKLMNOPQRSTUVWXYZ.abcdefghijklmnopqrstuvwxyz1234567890>,!':-+"

// Create the turn list
global.unit_list = ds_list_create();
global.unit_index = 0;

// Create the default font
global.font = font_add_sprite_ext(s_font, FONT_STRING, true, 1);
draw_set_font(global.font);

// Create the unit grid
global.unit_grid = ds_grid_create(room_width div CELL_SIZE, room_height div CELL_SIZE);
ds_grid_clear(global.unit_grid, noone);

window_set_cursor(cr_none);
cursor = noone;

audio_play_sound(a_music, 6, true);

// Set the gui size
display_set_gui_size(camera_get_view_width(view_camera[0]), camera_get_view_height(view_camera[0]));
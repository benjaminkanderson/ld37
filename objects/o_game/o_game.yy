{
    "id": "5897d123-6c00-4f84-a09d-7d300c78afee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_game",
    "eventList": [
        {
            "id": "482b4fdf-4e45-446a-89eb-248a403c752e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5897d123-6c00-4f84-a09d-7d300c78afee"
        },
        {
            "id": "e4e02497-d64c-4c4d-96de-7f89ace1cd6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "5897d123-6c00-4f84-a09d-7d300c78afee"
        },
        {
            "id": "acde0db3-8fd8-4ab5-9fae-c102f05ab5ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5897d123-6c00-4f84-a09d-7d300c78afee"
        },
        {
            "id": "c91ffdd7-269f-4d80-b127-777ced7615e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5897d123-6c00-4f84-a09d-7d300c78afee"
        }
    ],
    "maskSpriteId": "c9717d3b-a1e6-4fae-bc1f-f9a77da733b3",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
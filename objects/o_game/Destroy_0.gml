/// @description Destroy the unit list
ds_list_destroy(global.unit_list);
ds_grid_destroy(global.unit_grid);
if (cursor != noone) sprite_delete(cursor);
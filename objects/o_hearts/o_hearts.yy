{
    "id": "55ff3cea-56cd-4cd0-b292-5cba886acca6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hearts",
    "eventList": [
        {
            "id": "4c478ec8-f20f-4b0d-b363-87df5469d286",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "55ff3cea-56cd-4cd0-b292-5cba886acca6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "bfa656e1-5fba-4bc8-90e0-9e00c506e63b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
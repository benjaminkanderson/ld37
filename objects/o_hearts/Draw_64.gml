/// @description Draw the health
if (instance_exists(o_wizard)) {
	var _separation = 9;
	for (var _i=0; _i<o_wizard.max_hp; _i++) {
		draw_sprite(s_hearts, 1, ui_x+_separation*_i, ui_y);
		if (_i < o_wizard.hp) {
			draw_sprite(s_hearts, 0, ui_x+_separation*_i, ui_y);
		}
	}
} else {
	instance_destroy();
}
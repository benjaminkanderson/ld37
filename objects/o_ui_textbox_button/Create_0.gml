event_inherited();

margin = 8;
text = ">";
width = string_width(text)+margin;
height = string_height(text)+margin;
action = noone;
active = true;
textbox = noone;

// Update the scale
image_xscale = width/sprite_get_width(sprite_index);
image_yscale = height/sprite_get_height(sprite_index);
{
    "id": "ff3d2a33-be36-4d65-843e-47c7e9e547ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ui_textbox_button",
    "eventList": [
        {
            "id": "2c74adab-3579-44a2-a39a-a52eb8351515",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ff3d2a33-be36-4d65-843e-47c7e9e547ec"
        },
        {
            "id": "af9598f1-da01-4baf-bc57-3789b26709a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ff3d2a33-be36-4d65-843e-47c7e9e547ec"
        },
        {
            "id": "cf12dba5-7702-4131-b5a6-c2a54bc7ac16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ff3d2a33-be36-4d65-843e-47c7e9e547ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "bfa656e1-5fba-4bc8-90e0-9e00c506e63b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "70bcf39b-7911-4977-8757-8d4ecc83a6e4",
    "visible": true
}
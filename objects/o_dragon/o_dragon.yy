{
    "id": "a5263d03-f7b0-4563-8e64-ead33668c1c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_dragon",
    "eventList": [
        {
            "id": "fd368140-43e4-4dd1-9656-a5dc739b2898",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a5263d03-f7b0-4563-8e64-ead33668c1c3"
        }
    ],
    "maskSpriteId": "c9717d3b-a1e6-4fae-bc1f-f9a77da733b3",
    "parentObjectId": "78ab00f8-9588-4a72-9d27-3a0613748aab",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "3207841c-26a1-42d1-b618-c7b42f3df5ac",
    "visible": true
}
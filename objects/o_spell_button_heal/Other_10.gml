/// @description Heal the player
if (!instance_exists(o_wizard)) exit;
with (o_wizard) {
	hp = min(hp+2, max_hp);
	with (o_wizard) {
		audio_play_sound(a_heal, 5, false);
		var _text = instance_create_layer(x+CELL_SIZE/2, y-CELL_SIZE/2, "Instances", o_text);
		_text.image_blend = make_color_hsv(150, 160, 255);
		_text.text = "+"+string(2);
		repeat(10) {
			instance_create_layer(x+random(CELL_SIZE), y+random(CELL_SIZE), "Instances", o_heal_spell_effect);
		}
	}
}
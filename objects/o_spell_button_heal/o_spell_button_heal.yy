{
    "id": "e30c612e-b2fe-4c51-b251-f8e6d71b579d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_spell_button_heal",
    "eventList": [
        {
            "id": "a2b49daf-fbc3-4fbb-9d64-da64ff9e1f5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e30c612e-b2fe-4c51-b251-f8e6d71b579d"
        },
        {
            "id": "6b650c9f-8f4b-459a-99fc-8ba0dd6decc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e30c612e-b2fe-4c51-b251-f8e6d71b579d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "325cdb38-6263-48d2-82c1-01c4433d7122",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "088e1727-7335-4c6b-8fe5-0b27c863e833",
    "visible": true
}
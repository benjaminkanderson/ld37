/// @description Initialize the wizard
// Create the unit variables
yoffset = 0;
JUMP_HEIGHT = 8;
SPEED = 2;
hp = 2;
att = 1;
shake = 0;

// Turn constants
END_TURN = 0;

// Create the state variables
state = wait_state;
state_arguments = [];

// Set the location in the grid
ds_grid_set(global.unit_grid, x div CELL_SIZE, y div CELL_SIZE, id);
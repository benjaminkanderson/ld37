/// @description Execute the state
script_execute(state, state_arguments);

// Update the depth
depth = -y;

// Are we active?
var _index = ds_list_find_index(global.unit_list, id);
if (instance_exists(o_wizard)) {
	if (in_view() && _index == -1) {
		ds_list_add(global.unit_list, id);
	}
}

// Remove from the list
if (!in_view() && _index != -1) {
	ds_list_delete(global.unit_list, _index);
}

// Check for death
if (hp <= 0) {
	instance_destroy();
}
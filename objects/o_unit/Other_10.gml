/// @description End Turn


++global.unit_index;
if (global.unit_index > ds_list_size(global.unit_list)-1) global.unit_index = 0;
change_states(wait_state, [0]);
if (!instance_exists(o_cutscene)) {
	activate_next_unit();
}

if (object_index == o_bat) {
	moves = 1;
}
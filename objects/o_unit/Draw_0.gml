/// @description Custom draw
// Draw the shadow
var _shadow_scale = 1-.5/(JUMP_HEIGHT/yoffset);
draw_sprite_ext(s_shadow, 0, x+CELL_SIZE/2, y+CELL_SIZE/2, _shadow_scale, _shadow_scale, 0, c_white, 1);

// Draw the player
draw_sprite_ext(sprite_index, image_index, x+CELL_SIZE/2+random_range(-shake, shake), y-yoffset+random_range(-shake, shake), image_xscale, image_yscale, image_angle, image_blend, image_alpha);

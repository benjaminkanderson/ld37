{
    "id": "78ab00f8-9588-4a72-9d27-3a0613748aab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_unit",
    "eventList": [
        {
            "id": "0fb7e27e-b179-42c5-98ff-f4d8e1aa4de5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "78ab00f8-9588-4a72-9d27-3a0613748aab"
        },
        {
            "id": "8b87fe5a-7da2-4bac-83f1-36b2381dd0c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "78ab00f8-9588-4a72-9d27-3a0613748aab"
        },
        {
            "id": "d6903bf4-cc22-4d70-ac27-48e9b9b0b0e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "78ab00f8-9588-4a72-9d27-3a0613748aab"
        },
        {
            "id": "64432eff-c48c-4b26-bc17-935498c10668",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "78ab00f8-9588-4a72-9d27-3a0613748aab"
        },
        {
            "id": "6ded21d6-54d1-4248-959d-aa484b95d8ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "78ab00f8-9588-4a72-9d27-3a0613748aab"
        },
        {
            "id": "dd17a165-64b7-4144-aae9-c924db131f01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "78ab00f8-9588-4a72-9d27-3a0613748aab"
        }
    ],
    "maskSpriteId": "c9717d3b-a1e6-4fae-bc1f-f9a77da733b3",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
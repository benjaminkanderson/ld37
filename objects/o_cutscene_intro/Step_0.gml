/// @description Run the cutscene
switch (action) {
	case 0: cutscene_create_textbox_at_center("Welcome to the\narchwizard's arena.", cutscene_next_action); break;
	case 1: cutscene_create_textbox_at_center("He has summoned\nmonsters to kill you.", cutscene_next_action); break;
	case 2: cutscene_create_textbox_at_center("If you are careful,\nyou can outsmart them.", cutscene_next_action); break;
	case 3: cutscene_create_textbox_at_center("Good luck!", cutscene_next_action); break;
	case 4: instance_destroy(); activate_next_unit(); break;
}

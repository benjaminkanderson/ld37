{
    "id": "f666d5db-c351-4f48-817c-9af129f551ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_cutscene_intro",
    "eventList": [
        {
            "id": "c320def5-7920-433c-ac44-792347c82e0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f666d5db-c351-4f48-817c-9af129f551ac"
        },
        {
            "id": "a54744ad-784f-4df6-a480-a16090c731fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f666d5db-c351-4f48-817c-9af129f551ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "c2324b19-9d66-4673-b778-35404b25093e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
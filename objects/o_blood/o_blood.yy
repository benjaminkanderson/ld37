{
    "id": "32402e7b-efa6-4dad-9df6-7ddc6fe7dd82",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_blood",
    "eventList": [
        {
            "id": "9561e9fa-7370-47a4-94f4-f4fd703101b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32402e7b-efa6-4dad-9df6-7ddc6fe7dd82"
        },
        {
            "id": "b33d3d3d-08ff-48a7-b350-4a3979ba2152",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "32402e7b-efa6-4dad-9df6-7ddc6fe7dd82"
        },
        {
            "id": "7fd826f3-af4a-4693-95cb-b1d81fe6de2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "32402e7b-efa6-4dad-9df6-7ddc6fe7dd82"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "02802fea-63c5-4943-8677-4d3fca462dca",
    "visible": true
}
/// @description Destroy the blood
if (speed != 0) exit;
if (image_alpha > 0) {
	image_alpha -= .05;
	var _xsign = sign(image_xscale);
	var _ysign = sign(image_yscale);
	image_xscale += .05*_xsign;
	image_yscale += .02*_ysign;
} else {
	instance_destroy();
}
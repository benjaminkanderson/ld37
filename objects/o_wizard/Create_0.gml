event_inherited();
hp = 4;
max_hp = hp;
energy = 2;
att = 1;
max_energy = energy;
UPDATE_SPELL_LIST = 1;
image_speed = .7;

// Create the hearts object
instance_create_layer(6, 6, "Instances", o_hearts);
instance_create_layer(camera_get_view_width(view_camera[0])-6, 5, "Instances", o_crystals);

// Spell list
spells = ds_list_create();
ds_list_add(spells, o_spell_button_heal);
ds_list_add(spells, o_spell_button_fireball);

alarm[1] = seconds_to_steps(.5);
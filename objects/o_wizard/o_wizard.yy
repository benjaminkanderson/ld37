{
    "id": "27f6f7e9-cc3b-47c7-afa9-30d6e511e9ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_wizard",
    "eventList": [
        {
            "id": "0c71d391-926d-455e-80ff-df874f40a029",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27f6f7e9-cc3b-47c7-afa9-30d6e511e9ee"
        },
        {
            "id": "81c89313-55dd-4c74-a4c4-c9d90eac7777",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "27f6f7e9-cc3b-47c7-afa9-30d6e511e9ee"
        },
        {
            "id": "80a0ce0e-5d86-4587-9132-8394005e2455",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "27f6f7e9-cc3b-47c7-afa9-30d6e511e9ee"
        },
        {
            "id": "62861b70-2dc6-4657-b811-1e1d1281c75c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "27f6f7e9-cc3b-47c7-afa9-30d6e511e9ee"
        }
    ],
    "maskSpriteId": "c9717d3b-a1e6-4fae-bc1f-f9a77da733b3",
    "parentObjectId": "78ab00f8-9588-4a72-9d27-3a0613748aab",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4e13926c-3d41-45c3-9e90-858280a857b7",
    "visible": true
}
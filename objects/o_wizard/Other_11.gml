/// @description Update the spell list
for (var _i=0; _i<ds_list_size(spells); _i++) {
	var _spell = spells[| _i];
	if (!instance_exists(_spell)) {
		instance_create_layer(4+_i*13, camera_get_view_height(view_camera[0])-16, "Instances", _spell);
	}
}
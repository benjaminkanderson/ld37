/// @description Make the camera follow the player
if (instance_exists(o_wizard)) {
	var _target_x = o_wizard.x+CELL_SIZE/2;
	var _target_y = o_wizard.y+CELL_SIZE/2;
} else if (instance_exists(o_wizard_fall_in)) {
	var _target_x = o_wizard_fall_in.x+CELL_SIZE/2;
	var _target_y = o_wizard_fall_in.y+CELL_SIZE/2-o_wizard_fall_in.height;
} else exit;

// Move the camera
x = lerp(x, _target_x-camera_get_view_width(view_camera[0])/2, .1);
y = lerp(y, _target_y-camera_get_view_height(view_camera[0])/2, .1);
camera_set_view_pos(view_camera[0], x+random_range(-shake, shake), y+random_range(-shake, shake));

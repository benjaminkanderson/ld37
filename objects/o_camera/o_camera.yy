{
    "id": "ecc23cef-0dcf-4974-9e7a-0372fd56f34b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_camera",
    "eventList": [
        {
            "id": "d638fad7-a8b8-4e7c-adbe-c643d66c9b57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ecc23cef-0dcf-4974-9e7a-0372fd56f34b"
        },
        {
            "id": "090bbafd-58b2-490d-9bc3-bcce0d8ebeb0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ecc23cef-0dcf-4974-9e7a-0372fd56f34b"
        },
        {
            "id": "341ff74b-926d-4a63-9723-48e9dca71dd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ecc23cef-0dcf-4974-9e7a-0372fd56f34b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
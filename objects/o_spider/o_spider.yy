{
    "id": "d7740f5f-31fb-488f-9166-81082ca5cf85",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_spider",
    "eventList": [
        {
            "id": "155fb01c-d47d-4d7d-8e8d-62ae0ae89c38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d7740f5f-31fb-488f-9166-81082ca5cf85"
        }
    ],
    "maskSpriteId": "c9717d3b-a1e6-4fae-bc1f-f9a77da733b3",
    "parentObjectId": "78ab00f8-9588-4a72-9d27-3a0613748aab",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "5ba98b15-88eb-4fed-a86f-499a35a5fb62",
    "visible": true
}
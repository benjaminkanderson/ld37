{
    "id": "ff5e2056-d273-4c44-b098-5a2d9de3ffad",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_text",
    "eventList": [
        {
            "id": "9d8a5111-d05c-41f3-baab-c3237ab8216e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ff5e2056-d273-4c44-b098-5a2d9de3ffad"
        },
        {
            "id": "8cd68a53-cdad-4e27-81bd-b56158125c54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ff5e2056-d273-4c44-b098-5a2d9de3ffad"
        },
        {
            "id": "9c1212c0-53eb-4fec-ab5b-7bf1fc60578e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ff5e2056-d273-4c44-b098-5a2d9de3ffad"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
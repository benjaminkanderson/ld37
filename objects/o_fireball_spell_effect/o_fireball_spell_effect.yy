{
    "id": "0df6edb1-e9ba-46ff-8289-5f15e3bb0ad5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_fireball_spell_effect",
    "eventList": [
        {
            "id": "4f49be29-3a41-49c4-8cc0-18dc78c4f4b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0df6edb1-e9ba-46ff-8289-5f15e3bb0ad5"
        },
        {
            "id": "984f454b-664f-4531-a3f2-b209549d5bec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0df6edb1-e9ba-46ff-8289-5f15e3bb0ad5"
        },
        {
            "id": "570ffc7f-1f26-4953-9d62-f923199d3448",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0df6edb1-e9ba-46ff-8289-5f15e3bb0ad5"
        },
        {
            "id": "d9edb4bd-fd99-4732-be23-041980bd08bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0df6edb1-e9ba-46ff-8289-5f15e3bb0ad5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "204710c0-fb09-4e8d-8c6c-9f6f563f9359",
    "visible": true
}
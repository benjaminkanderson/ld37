gravity = -random(.2);
image_index = choose(0, 1);
image_speed = 0;
depth = -y*3;
image_xscale = choose(1, -1)*random_range(.5, 1);
image_yscale = choose(1, -1)*random_range(.5, 1);
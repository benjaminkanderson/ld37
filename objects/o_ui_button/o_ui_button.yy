{
    "id": "ec633b42-112e-4be1-84e6-83a527c9d6c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ui_button",
    "eventList": [
        {
            "id": "0ff6106c-95cf-40ff-8b5f-5db2b29bc3bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ec633b42-112e-4be1-84e6-83a527c9d6c2"
        },
        {
            "id": "1628e7d5-f389-4df7-95a1-c9df4841d85b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ec633b42-112e-4be1-84e6-83a527c9d6c2"
        },
        {
            "id": "67af3d91-c06e-408c-b738-a84cf90595b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ec633b42-112e-4be1-84e6-83a527c9d6c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "bfa656e1-5fba-4bc8-90e0-9e00c506e63b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "70bcf39b-7911-4977-8757-8d4ecc83a6e4",
    "visible": true
}
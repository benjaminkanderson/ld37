event_inherited();

margin = 8;
text = "Press this button.";
width = string_width(text)+margin;
height = string_height(text)+margin;
action = noone;
active = true;

// Update the scale
image_xscale = width/sprite_get_width(sprite_index);
image_yscale = height/sprite_get_height(sprite_index);
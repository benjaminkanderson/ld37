// Draw the shadow
var _shadow_scale = 1-.5/(64/height);
draw_sprite_ext(s_shadow, 0, x+CELL_SIZE/2, y+CELL_SIZE/2, _shadow_scale, _shadow_scale, 0, c_white, 1);

draw_sprite_ext(sprite_index, image_index, x+CELL_SIZE/2, y-height, image_xscale, image_yscale, image_angle, image_blend, image_alpha);
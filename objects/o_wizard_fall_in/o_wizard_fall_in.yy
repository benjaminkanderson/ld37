{
    "id": "69b83b69-4c8f-4720-b735-055760e25e35",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_wizard_fall_in",
    "eventList": [
        {
            "id": "612e9a20-5cf5-48ea-b912-b9da3a21829b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "69b83b69-4c8f-4720-b735-055760e25e35"
        },
        {
            "id": "9c41e373-6ca4-4492-a98b-f8b80be424f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "69b83b69-4c8f-4720-b735-055760e25e35"
        },
        {
            "id": "e45dfca7-ec5a-4d69-9238-bf76ed7abbba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "69b83b69-4c8f-4720-b735-055760e25e35"
        },
        {
            "id": "5cbc9a82-8a45-457d-ac86-3a9ff53f1162",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "69b83b69-4c8f-4720-b735-055760e25e35"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4e13926c-3d41-45c3-9e90-858280a857b7",
    "visible": true
}
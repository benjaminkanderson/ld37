vspd += grav;
height = max(0, height-vspd);

if (height == 0 && alarm[0] = -1) {
	set_shake(o_camera, 2, seconds_to_steps(.4));
	alarm[0] = seconds_to_steps(.5);
	audio_play_sound(a_land, 5, false);
}
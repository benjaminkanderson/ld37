/// @description Run the cutscene
switch (action) {
	case 0: cutscene_create_textbox_at_center("You win!", cutscene_next_action); break;
	case 1: cutscene_create_textbox_at_center("Having defeated all\nthe monsters you\nare able to teleport\naway.", cutscene_next_action); break;
	case 2: game_end();
}

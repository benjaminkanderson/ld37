{
    "id": "76f713a6-1bcc-454e-ac5b-8a85cef38015",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_cutscene_end",
    "eventList": [
        {
            "id": "92fb5d92-f2d9-46e0-abeb-06f160aae2a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "76f713a6-1bcc-454e-ac5b-8a85cef38015"
        },
        {
            "id": "d9dab064-d7b1-4c3b-a169-516d454c46e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "76f713a6-1bcc-454e-ac5b-8a85cef38015"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "c2324b19-9d66-4673-b778-35404b25093e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
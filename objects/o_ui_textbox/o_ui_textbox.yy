{
    "id": "efee625a-7f30-4f48-bb0a-4b87344f9be9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ui_textbox",
    "eventList": [
        {
            "id": "451c787f-d9e0-4758-b4af-a3e3c6f2f6e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "efee625a-7f30-4f48-bb0a-4b87344f9be9"
        },
        {
            "id": "215ae331-96ed-462e-ae6c-115db1c92300",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "efee625a-7f30-4f48-bb0a-4b87344f9be9"
        },
        {
            "id": "10e8eeb5-e06d-4985-a225-78d10f31c80e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "efee625a-7f30-4f48-bb0a-4b87344f9be9"
        }
    ],
    "maskSpriteId": "37551299-ea7e-41a5-80a3-f5c1c9f71a42",
    "parentObjectId": "bfa656e1-5fba-4bc8-90e0-9e00c506e63b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "37551299-ea7e-41a5-80a3-f5c1c9f71a42",
    "visible": true
}
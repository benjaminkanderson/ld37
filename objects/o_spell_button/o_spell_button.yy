{
    "id": "325cdb38-6263-48d2-82c1-01c4433d7122",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_spell_button",
    "eventList": [
        {
            "id": "f3969d8d-82c9-4528-92e1-6b4843a1d704",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "325cdb38-6263-48d2-82c1-01c4433d7122"
        },
        {
            "id": "5af2415c-a04e-4728-91c5-88124627a3b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "325cdb38-6263-48d2-82c1-01c4433d7122"
        },
        {
            "id": "30743d00-ab29-448b-af56-6b5802142541",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "325cdb38-6263-48d2-82c1-01c4433d7122"
        },
        {
            "id": "5576bf22-d509-418d-b1b5-5f1276572131",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "325cdb38-6263-48d2-82c1-01c4433d7122"
        },
        {
            "id": "7ed15201-3aea-4ba6-9710-9a78a994ba7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "325cdb38-6263-48d2-82c1-01c4433d7122"
        },
        {
            "id": "f3aca5e5-b16f-4c3e-a8b3-ae1bed9ecc30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "325cdb38-6263-48d2-82c1-01c4433d7122"
        },
        {
            "id": "da2dcde7-f0df-4e21-bf0c-8d00bf94d89b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "325cdb38-6263-48d2-82c1-01c4433d7122"
        },
        {
            "id": "5fbec0c6-857b-468e-8d77-9182c4f33fed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "325cdb38-6263-48d2-82c1-01c4433d7122"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "ec633b42-112e-4be1-84e6-83a527c9d6c2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "70bcf39b-7911-4977-8757-8d4ecc83a6e4",
    "visible": true
}
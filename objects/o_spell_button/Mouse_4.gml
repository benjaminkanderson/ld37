/// @description Runt he action
if (!instance_exists(o_wizard)) exit;
if (visible && active && o_wizard.energy >= cost) {
	event_user(SPELL_ACTION);
	o_wizard.energy -= cost;
	alarm[0] = 4;
}
if (instance_exists(tool_tip)) instance_destroy(tool_tip);
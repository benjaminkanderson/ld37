if (!instance_exists(o_wizard)) exit;
with (o_wizard) event_user(END_TURN);
if (instance_exists(o_action_cue)) instance_destroy(o_action_cue);
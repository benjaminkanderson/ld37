if (!instance_exists(o_wizard)) exit;
active = o_wizard.state == player_action_state;
if (instance_exists(tool_tip)) {
	tool_tip.visible = active;
}
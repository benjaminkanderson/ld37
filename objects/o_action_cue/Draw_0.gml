/// @description Draw the visual cues
for (var j=0; j<3; j++) {
	for (var i=0; i<3; i++) {
		var _x = (x-CELL_SIZE)+i*CELL_SIZE;
		var _y = (y-CELL_SIZE)+j*CELL_SIZE;
		if (active_cues[i, j] == 1) {
			draw_sprite_ext(s_move_action_cue, 0, _x, _y, 1, 1, 0, c_white, .3);
		} else if (active_cues[i, j] == 2) {
			draw_sprite_ext(s_move_action_cue, 0, _x, _y, 1, 1, 0, c_red, .3);
		}
	}
}
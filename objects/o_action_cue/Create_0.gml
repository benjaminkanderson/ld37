/// @description Find visual cue for actions
for (var j=0; j<3; j++) {
	for (var i=0; i<3; i++) {
		if (j==1 && i==1) continue;
		var _x = (x-CELL_SIZE)+i*CELL_SIZE;
		var _y = (y-CELL_SIZE)+j*CELL_SIZE;
		active_cues[i, j] = grid_place_free(_x, _y) && point_distance(x, y, _x, _y) == CELL_SIZE;
		if (global.unit_grid[# _x/CELL_SIZE, _y/CELL_SIZE]) {
			active_cues[i, j] = 2;
		}
	}
}
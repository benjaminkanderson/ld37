{
    "id": "1ff65f4d-c31c-42f0-a0b7-8bf1c68123b9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_crystals",
    "eventList": [
        {
            "id": "7e786a41-4b20-46fd-9a9a-092962556882",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1ff65f4d-c31c-42f0-a0b7-8bf1c68123b9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "bfa656e1-5fba-4bc8-90e0-9e00c506e63b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
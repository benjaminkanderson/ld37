/// @description Draw the health
if (instance_exists(o_wizard)) {
	var _separation = 9;
	var _start_x = ui_x - o_wizard.max_energy-1*_separation;
	for (var _i=0; _i<o_wizard.max_energy; _i++) {
		draw_sprite(s_crystals, 1, _start_x+_separation*_i, ui_y);
		if (_i < o_wizard.energy) {
			draw_sprite(s_crystals, 0, _start_x+_separation*_i, ui_y);
		}
	}
} else {
	instance_destroy();
}
{
    "id": "bfa656e1-5fba-4bc8-90e0-9e00c506e63b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ui",
    "eventList": [
        {
            "id": "98749a6e-db55-4f79-8cb7-9eb28afaf09a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bfa656e1-5fba-4bc8-90e0-9e00c506e63b"
        },
        {
            "id": "1ca5d88a-48ea-4603-9140-8ce3b29bfbfe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "bfa656e1-5fba-4bc8-90e0-9e00c506e63b"
        },
        {
            "id": "19ba45d2-f69d-4b9a-9507-3aca459c7056",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "bfa656e1-5fba-4bc8-90e0-9e00c506e63b"
        },
        {
            "id": "7724e616-c647-422b-b041-0e2c888ed01c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bfa656e1-5fba-4bc8-90e0-9e00c506e63b"
        }
    ],
    "maskSpriteId": "37551299-ea7e-41a5-80a3-f5c1c9f71a42",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "37551299-ea7e-41a5-80a3-f5c1c9f71a42",
    "visible": true
}
{
    "id": "25935af3-2ebf-4d45-96eb-94700458a5e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_heal_spell_effect",
    "eventList": [
        {
            "id": "0bdb66e0-086f-4e52-b293-7a450829142a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "25935af3-2ebf-4d45-96eb-94700458a5e2"
        },
        {
            "id": "2050e937-ee13-46bd-b683-9786fec7f24b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "25935af3-2ebf-4d45-96eb-94700458a5e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "baac7a45-9400-42ee-92fc-dac0aeeb642d",
    "visible": true
}
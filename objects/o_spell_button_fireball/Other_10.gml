/// @description Heal the player
if (!instance_exists(o_wizard)) exit;
with (o_wizard) {
	var _grid_x = o_wizard.x/CELL_SIZE;
	var _grid_y = o_wizard.y/CELL_SIZE;
	for (var _dir=0; _dir<360; _dir+=90) {
		var _enemy_grid_x = _grid_x+lengthdir_x(1, _dir);
		var _enemy_grid_y = _grid_y+lengthdir_y(1, _dir);
		var enemy = global.unit_grid[# _enemy_grid_x, _enemy_grid_y];
		if (enemy != undefined && instance_exists(enemy)) {
			var _damage = att+1;
			if (irandom(4) == 4) _damage+=1;
			enemy.hp -= _damage;
			var _text = instance_create_layer(enemy.x+CELL_SIZE/2, enemy.y-CELL_SIZE/2, "Instances", o_text);
			_text.image_blend = make_color_hsv(0, 160, 255);
			_text.text = "-"+string(_damage);
			set_shake(enemy, 2, seconds_to_steps(.25));
			set_shake(o_camera, 2, seconds_to_steps(.2));
			create_blood(enemy.x+CELL_SIZE/2, enemy.y+CELL_SIZE/2, 8, 2);
		}
		repeat(8) {
			instance_create_layer(_enemy_grid_x*CELL_SIZE+random(CELL_SIZE), _enemy_grid_y*CELL_SIZE+random(CELL_SIZE), "Instances", o_fireball_spell_effect);
		}
		audio_play_sound(a_explode, 6, false);
	}
}
{
    "id": "422cc51b-0327-4fbb-89ed-2399658c8384",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_spell_button_fireball",
    "eventList": [
        {
            "id": "0bfae12e-55ce-47a9-b35b-bd3ce28a7c68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "422cc51b-0327-4fbb-89ed-2399658c8384"
        },
        {
            "id": "3caffc36-6abb-4f5f-bdae-c8409f624a16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "422cc51b-0327-4fbb-89ed-2399658c8384"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "325cdb38-6263-48d2-82c1-01c4433d7122",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "efe680e9-69ea-4f1b-be28-3c0087177468",
    "visible": true
}
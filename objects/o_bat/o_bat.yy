{
    "id": "47c0110e-c408-4fcc-bc22-7950f816f425",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bat",
    "eventList": [
        {
            "id": "1855ae9b-3d74-493b-8f4e-6fdcfbc3c2db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "47c0110e-c408-4fcc-bc22-7950f816f425"
        },
        {
            "id": "2724188b-ee22-44bf-aa6b-00a8223726a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "47c0110e-c408-4fcc-bc22-7950f816f425"
        }
    ],
    "maskSpriteId": "c9717d3b-a1e6-4fae-bc1f-f9a77da733b3",
    "parentObjectId": "78ab00f8-9588-4a72-9d27-3a0613748aab",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "99f34b2e-5843-4476-a58b-25b3da7c7739",
    "visible": true
}
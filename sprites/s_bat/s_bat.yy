{
    "id": "99f34b2e-5843-4476-a58b-25b3da7c7739",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_bat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "54950c87-7438-4131-908d-8a61600caa41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99f34b2e-5843-4476-a58b-25b3da7c7739",
            "compositeImage": {
                "id": "3897d470-23a0-425a-910e-ba6432e6b95c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54950c87-7438-4131-908d-8a61600caa41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2734f4d4-094d-4950-8fd2-cd01aaf01876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54950c87-7438-4131-908d-8a61600caa41",
                    "LayerId": "e22c3f9a-f8fb-47aa-82c1-8384c46e9f42"
                }
            ]
        },
        {
            "id": "735fdb39-7129-4d60-a334-b80b384c2c44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99f34b2e-5843-4476-a58b-25b3da7c7739",
            "compositeImage": {
                "id": "c2ebabfb-71e7-4683-a0b6-ffa6046b8f02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "735fdb39-7129-4d60-a334-b80b384c2c44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8efb64-61ba-414f-9b41-e8cba0ca3bf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "735fdb39-7129-4d60-a334-b80b384c2c44",
                    "LayerId": "e22c3f9a-f8fb-47aa-82c1-8384c46e9f42"
                }
            ]
        },
        {
            "id": "0cb49829-8297-45bc-b56e-cf749365d5ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99f34b2e-5843-4476-a58b-25b3da7c7739",
            "compositeImage": {
                "id": "89c33d3a-9336-4249-81bd-1984e90c9858",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cb49829-8297-45bc-b56e-cf749365d5ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f63e2308-16ab-4ea7-a82a-a04edbbada4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cb49829-8297-45bc-b56e-cf749365d5ac",
                    "LayerId": "e22c3f9a-f8fb-47aa-82c1-8384c46e9f42"
                }
            ]
        },
        {
            "id": "1f9bcfef-372a-4638-ac66-c6dbb37b1739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99f34b2e-5843-4476-a58b-25b3da7c7739",
            "compositeImage": {
                "id": "7d8e5a0f-bc92-4c0e-8ac0-783d17aaaa5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f9bcfef-372a-4638-ac66-c6dbb37b1739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9616370-fce0-4347-96c1-f9987613f92d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f9bcfef-372a-4638-ac66-c6dbb37b1739",
                    "LayerId": "e22c3f9a-f8fb-47aa-82c1-8384c46e9f42"
                }
            ]
        }
    ],
    "height": 16,
    "layers": [
        {
            "id": "e22c3f9a-f8fb-47aa-82c1-8384c46e9f42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99f34b2e-5843-4476-a58b-25b3da7c7739",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 4
}
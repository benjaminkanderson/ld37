{
    "id": "3207841c-26a1-42d1-b618-c7b42f3df5ac",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_dragon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e8c9bbe2-a32f-440f-82fd-f21d0b02e729",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3207841c-26a1-42d1-b618-c7b42f3df5ac",
            "compositeImage": {
                "id": "7501576e-2108-4f67-8f71-b268ce223693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8c9bbe2-a32f-440f-82fd-f21d0b02e729",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db1ebd83-90f4-480a-a79c-b1e2824980d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8c9bbe2-a32f-440f-82fd-f21d0b02e729",
                    "LayerId": "65ae7411-42fa-409e-9a95-001fc25691f8"
                },
                {
                    "id": "2c10ed52-8c7b-472f-9324-f7e098f65cb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8c9bbe2-a32f-440f-82fd-f21d0b02e729",
                    "LayerId": "77ded3dd-97bd-413d-aab1-75f6ca584222"
                },
                {
                    "id": "f8008283-407d-452f-b0e0-fab3db551f9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8c9bbe2-a32f-440f-82fd-f21d0b02e729",
                    "LayerId": "483f81f1-cdac-4065-81dd-9e6953faa6b7"
                },
                {
                    "id": "9c557c97-e0f3-4346-91fc-a7ff11a065ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8c9bbe2-a32f-440f-82fd-f21d0b02e729",
                    "LayerId": "d23878e8-3028-4624-b803-0e4e21659b97"
                }
            ]
        },
        {
            "id": "bc1b9365-3346-4bcd-b7d4-2583feef57ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3207841c-26a1-42d1-b618-c7b42f3df5ac",
            "compositeImage": {
                "id": "e7a0a2d1-4083-421f-8b0a-eb31bc0bc941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc1b9365-3346-4bcd-b7d4-2583feef57ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eda6829a-2ed8-493c-9c4d-a1795f51838d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1b9365-3346-4bcd-b7d4-2583feef57ea",
                    "LayerId": "d23878e8-3028-4624-b803-0e4e21659b97"
                },
                {
                    "id": "d52e4b0f-3e92-42c5-a4d8-3d348826b2fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1b9365-3346-4bcd-b7d4-2583feef57ea",
                    "LayerId": "77ded3dd-97bd-413d-aab1-75f6ca584222"
                },
                {
                    "id": "c140230b-c51b-43c6-8802-f897223eebe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1b9365-3346-4bcd-b7d4-2583feef57ea",
                    "LayerId": "65ae7411-42fa-409e-9a95-001fc25691f8"
                },
                {
                    "id": "6657feb8-f81d-470a-a06a-1ef3bc5399a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1b9365-3346-4bcd-b7d4-2583feef57ea",
                    "LayerId": "483f81f1-cdac-4065-81dd-9e6953faa6b7"
                }
            ]
        },
        {
            "id": "4bd0a277-2554-415e-9ead-6498c7065b9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3207841c-26a1-42d1-b618-c7b42f3df5ac",
            "compositeImage": {
                "id": "3e8eb72c-9f27-492e-b6bc-8575431ed6c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bd0a277-2554-415e-9ead-6498c7065b9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edfff8a9-4379-4dea-bb78-adf3cac09510",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd0a277-2554-415e-9ead-6498c7065b9f",
                    "LayerId": "d23878e8-3028-4624-b803-0e4e21659b97"
                },
                {
                    "id": "aa20e600-c328-42ae-9ad8-65a371c8e003",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd0a277-2554-415e-9ead-6498c7065b9f",
                    "LayerId": "77ded3dd-97bd-413d-aab1-75f6ca584222"
                },
                {
                    "id": "4d3905f7-c179-4590-8612-0da683893fcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd0a277-2554-415e-9ead-6498c7065b9f",
                    "LayerId": "65ae7411-42fa-409e-9a95-001fc25691f8"
                },
                {
                    "id": "45e61cb2-b057-4dd3-9723-c2770bfb7d07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd0a277-2554-415e-9ead-6498c7065b9f",
                    "LayerId": "483f81f1-cdac-4065-81dd-9e6953faa6b7"
                }
            ]
        },
        {
            "id": "bb9c38f6-e32c-4485-a00e-40ef773cf2e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3207841c-26a1-42d1-b618-c7b42f3df5ac",
            "compositeImage": {
                "id": "4f971c18-e2a5-4656-a0b4-e4e9c962c4b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb9c38f6-e32c-4485-a00e-40ef773cf2e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eff9ec37-f385-4dc9-afe2-643b62683ac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb9c38f6-e32c-4485-a00e-40ef773cf2e4",
                    "LayerId": "d23878e8-3028-4624-b803-0e4e21659b97"
                },
                {
                    "id": "1a827593-b0dc-4ed5-970c-2c47224f84a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb9c38f6-e32c-4485-a00e-40ef773cf2e4",
                    "LayerId": "77ded3dd-97bd-413d-aab1-75f6ca584222"
                },
                {
                    "id": "d5732d95-dde9-483b-8f0a-cb8c6ebdb463",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb9c38f6-e32c-4485-a00e-40ef773cf2e4",
                    "LayerId": "65ae7411-42fa-409e-9a95-001fc25691f8"
                },
                {
                    "id": "81992c67-3f7d-4422-b6cb-5ea57972e654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb9c38f6-e32c-4485-a00e-40ef773cf2e4",
                    "LayerId": "483f81f1-cdac-4065-81dd-9e6953faa6b7"
                }
            ]
        }
    ],
    "height": 24,
    "layers": [
        {
            "id": "d23878e8-3028-4624-b803-0e4e21659b97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3207841c-26a1-42d1-b618-c7b42f3df5ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "Head",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "77ded3dd-97bd-413d-aab1-75f6ca584222",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3207841c-26a1-42d1-b618-c7b42f3df5ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "Front Wing",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "65ae7411-42fa-409e-9a95-001fc25691f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3207841c-26a1-42d1-b618-c7b42f3df5ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "Dragon",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "483f81f1-cdac-4065-81dd-9e6953faa6b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3207841c-26a1-42d1-b618-c7b42f3df5ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "Back Wing",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 13
}
{
    "id": "efe680e9-69ea-4f1b-be28-3c0087177468",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_fireball_spell_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "284a6eb6-0359-4c9a-b8e7-a4111092293f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efe680e9-69ea-4f1b-be28-3c0087177468",
            "compositeImage": {
                "id": "fcc9f614-51ea-47c1-a659-4666c1d9ede3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "284a6eb6-0359-4c9a-b8e7-a4111092293f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "017c9688-a763-4c5b-98f7-27355ca22241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "284a6eb6-0359-4c9a-b8e7-a4111092293f",
                    "LayerId": "28ae35ee-d490-4a6c-ab73-dd79dd1b2548"
                }
            ]
        },
        {
            "id": "b5469924-da2c-4bef-bff3-52c5b2ea6175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efe680e9-69ea-4f1b-be28-3c0087177468",
            "compositeImage": {
                "id": "27fb2680-aadc-44e2-a44c-daae45adf26e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5469924-da2c-4bef-bff3-52c5b2ea6175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2af8b810-5c77-40ed-9051-6305f6405908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5469924-da2c-4bef-bff3-52c5b2ea6175",
                    "LayerId": "28ae35ee-d490-4a6c-ab73-dd79dd1b2548"
                }
            ]
        }
    ],
    "height": 12,
    "layers": [
        {
            "id": "28ae35ee-d490-4a6c-ab73-dd79dd1b2548",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efe680e9-69ea-4f1b-be28-3c0087177468",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "70bcf39b-7911-4977-8757-8d4ecc83a6e4",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5226dfc4-24cb-4466-acae-03ca4970b514",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bcf39b-7911-4977-8757-8d4ecc83a6e4",
            "compositeImage": {
                "id": "b6668647-e323-4810-b2d0-eccec0fa9bd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5226dfc4-24cb-4466-acae-03ca4970b514",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cac8bcd9-0ef9-4999-9ce9-756d25acac49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5226dfc4-24cb-4466-acae-03ca4970b514",
                    "LayerId": "e49c9692-d9fc-4085-b133-289178d6b7b3"
                }
            ]
        },
        {
            "id": "0bbe054d-795f-4e1a-a731-bf4fcd098231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bcf39b-7911-4977-8757-8d4ecc83a6e4",
            "compositeImage": {
                "id": "501ae856-f154-4fc7-a197-795d0659d7c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbe054d-795f-4e1a-a731-bf4fcd098231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c391e6b-8c06-4029-ab00-528347f83614",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbe054d-795f-4e1a-a731-bf4fcd098231",
                    "LayerId": "e49c9692-d9fc-4085-b133-289178d6b7b3"
                }
            ]
        }
    ],
    "height": 12,
    "layers": [
        {
            "id": "e49c9692-d9fc-4085-b133-289178d6b7b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70bcf39b-7911-4977-8757-8d4ecc83a6e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}
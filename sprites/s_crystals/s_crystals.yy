{
    "id": "cebc89b3-669d-4014-b14f-558ff560ad4d",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_crystals",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e0c17d9e-70ce-4374-8a78-422d3ad6786e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cebc89b3-669d-4014-b14f-558ff560ad4d",
            "compositeImage": {
                "id": "53b200ec-9297-4516-9388-16f7d27af8ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0c17d9e-70ce-4374-8a78-422d3ad6786e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc4fbd69-f78d-4931-8da5-5905e6c898db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0c17d9e-70ce-4374-8a78-422d3ad6786e",
                    "LayerId": "c8e27ef1-26be-489c-b4d6-7ae098f9eeec"
                }
            ]
        },
        {
            "id": "ed9be83d-6cf8-4aaf-9580-73002dc62c63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cebc89b3-669d-4014-b14f-558ff560ad4d",
            "compositeImage": {
                "id": "9ddfb28c-bbb5-4ded-a2b0-53aa30f57dbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed9be83d-6cf8-4aaf-9580-73002dc62c63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5888bdb5-f17a-4100-b3b1-55853986a8b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed9be83d-6cf8-4aaf-9580-73002dc62c63",
                    "LayerId": "c8e27ef1-26be-489c-b4d6-7ae098f9eeec"
                }
            ]
        }
    ],
    "height": 9,
    "layers": [
        {
            "id": "c8e27ef1-26be-489c-b4d6-7ae098f9eeec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cebc89b3-669d-4014-b14f-558ff560ad4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}
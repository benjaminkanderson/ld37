{
    "id": "914fab31-c875-4592-9f16-750fa625874b",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4ab1a95c-772e-4dac-8e48-1c6fd58bba30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "914fab31-c875-4592-9f16-750fa625874b",
            "compositeImage": {
                "id": "d386aaab-9155-4585-a3cb-d4adcd07475a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ab1a95c-772e-4dac-8e48-1c6fd58bba30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a630107-7db2-4620-9267-9d6cdbb1c9ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ab1a95c-772e-4dac-8e48-1c6fd58bba30",
                    "LayerId": "fe71792f-d52a-4001-a267-e3a52b311012"
                }
            ]
        }
    ],
    "height": 32,
    "layers": [
        {
            "id": "fe71792f-d52a-4001-a267-e3a52b311012",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "914fab31-c875-4592-9f16-750fa625874b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}
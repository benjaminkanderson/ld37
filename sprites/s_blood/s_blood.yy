{
    "id": "02802fea-63c5-4943-8677-4d3fca462dca",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_blood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fcddbf09-e8cf-4102-a6ea-c81085756579",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02802fea-63c5-4943-8677-4d3fca462dca",
            "compositeImage": {
                "id": "63a66035-cf1d-4d25-8f0f-ca51448d99cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcddbf09-e8cf-4102-a6ea-c81085756579",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bdece78-c1f6-4882-9885-5159fce7523b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcddbf09-e8cf-4102-a6ea-c81085756579",
                    "LayerId": "d8248e0e-2810-46f8-9240-f93b13be177a"
                }
            ]
        },
        {
            "id": "f9030393-9ee1-4d7f-ad53-0da316d1304b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02802fea-63c5-4943-8677-4d3fca462dca",
            "compositeImage": {
                "id": "0cc90232-f33d-4e5b-b2a7-fa10a5ad1c99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9030393-9ee1-4d7f-ad53-0da316d1304b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d4efcae-f5b0-4bcd-bda0-1d80b2a89535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9030393-9ee1-4d7f-ad53-0da316d1304b",
                    "LayerId": "d8248e0e-2810-46f8-9240-f93b13be177a"
                }
            ]
        },
        {
            "id": "8cc342ae-6538-4130-bba6-9e0239b8e986",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02802fea-63c5-4943-8677-4d3fca462dca",
            "compositeImage": {
                "id": "e96b780d-0460-4856-8948-5d0620a986f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cc342ae-6538-4130-bba6-9e0239b8e986",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b109176-7626-4332-8736-e911adaaf429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cc342ae-6538-4130-bba6-9e0239b8e986",
                    "LayerId": "d8248e0e-2810-46f8-9240-f93b13be177a"
                }
            ]
        }
    ],
    "height": 8,
    "layers": [
        {
            "id": "d8248e0e-2810-46f8-9240-f93b13be177a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02802fea-63c5-4943-8677-4d3fca462dca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}
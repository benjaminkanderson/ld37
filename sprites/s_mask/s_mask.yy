{
    "id": "c9717d3b-a1e6-4fae-bc1f-f9a77da733b3",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d5d3cf84-413e-4d16-aed7-33348f36d6fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9717d3b-a1e6-4fae-bc1f-f9a77da733b3",
            "compositeImage": {
                "id": "d1d9227f-c916-4031-ba67-0d46d43c30ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5d3cf84-413e-4d16-aed7-33348f36d6fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f404970c-d9db-480b-8177-50250165f245",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5d3cf84-413e-4d16-aed7-33348f36d6fc",
                    "LayerId": "c36c4255-30fe-4c1f-b82b-5160e17f976a"
                }
            ]
        }
    ],
    "height": 16,
    "layers": [
        {
            "id": "c36c4255-30fe-4c1f-b82b-5160e17f976a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9717d3b-a1e6-4fae-bc1f-f9a77da733b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "b3fb0590-3c92-4a6f-8706-0fdb354fe264",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "535626cf-0c9a-4c91-8d28-1e27e0eeca28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3fb0590-3c92-4a6f-8706-0fdb354fe264",
            "compositeImage": {
                "id": "b4534345-647d-4b11-b8ec-b9e5520ce3d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "535626cf-0c9a-4c91-8d28-1e27e0eeca28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bfeb22a-1da2-43ad-bd1f-27d189adfa83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "535626cf-0c9a-4c91-8d28-1e27e0eeca28",
                    "LayerId": "29cbe21e-9038-4b9c-8ed4-ad937dbbf4ae"
                }
            ]
        }
    ],
    "height": 8,
    "layers": [
        {
            "id": "29cbe21e-9038-4b9c-8ed4-ad937dbbf4ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3fb0590-3c92-4a6f-8706-0fdb354fe264",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}
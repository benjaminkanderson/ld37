{
    "id": "204710c0-fb09-4e8d-8c6c-9f6f563f9359",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_fireball_spell_effect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f0e268a2-81a3-445b-a20e-fa9a9378f113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "204710c0-fb09-4e8d-8c6c-9f6f563f9359",
            "compositeImage": {
                "id": "8a0cdbf7-ee4d-4fb2-974e-20816f86ffc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0e268a2-81a3-445b-a20e-fa9a9378f113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ee7b1d7-3060-4d05-9715-a08a97bdb114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0e268a2-81a3-445b-a20e-fa9a9378f113",
                    "LayerId": "fe13d995-d51b-4e77-a66c-5679e5c130d0"
                }
            ]
        }
    ],
    "height": 12,
    "layers": [
        {
            "id": "fe13d995-d51b-4e77-a66c-5679e5c130d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "204710c0-fb09-4e8d-8c6c-9f6f563f9359",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "088e1727-7335-4c6b-8fe5-0b27c863e833",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_heal_spell_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f1b87d13-ae74-4753-879c-a573a5167cc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "088e1727-7335-4c6b-8fe5-0b27c863e833",
            "compositeImage": {
                "id": "41db8e0d-93f7-4a14-acea-713e8feaf67c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1b87d13-ae74-4753-879c-a573a5167cc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e78df9e-0abb-4684-9c56-6139981a810c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1b87d13-ae74-4753-879c-a573a5167cc5",
                    "LayerId": "f1495f62-6bdf-4ce1-95d7-7f1c08e94f2e"
                }
            ]
        },
        {
            "id": "098bab3d-fd8b-4715-9d4d-c0dcd5677b7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "088e1727-7335-4c6b-8fe5-0b27c863e833",
            "compositeImage": {
                "id": "1a68a3a3-a52a-4137-8f56-368623345da2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "098bab3d-fd8b-4715-9d4d-c0dcd5677b7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef9a301a-c83a-44dc-80a1-f60a1ec6ab0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "098bab3d-fd8b-4715-9d4d-c0dcd5677b7c",
                    "LayerId": "f1495f62-6bdf-4ce1-95d7-7f1c08e94f2e"
                }
            ]
        }
    ],
    "height": 12,
    "layers": [
        {
            "id": "f1495f62-6bdf-4ce1-95d7-7f1c08e94f2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "088e1727-7335-4c6b-8fe5-0b27c863e833",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_font",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "eca8ceab-a169-4f2a-abbf-44621b465e2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "15208eef-3115-4a46-a8f3-df36d74f9632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eca8ceab-a169-4f2a-abbf-44621b465e2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0853b2c-e8c9-446d-b794-0e640d05e2e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eca8ceab-a169-4f2a-abbf-44621b465e2b",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "01042e44-bb33-4963-a0b8-f498a83a5e61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "0cfabeaa-0c31-4212-8cd6-2d24034ae964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01042e44-bb33-4963-a0b8-f498a83a5e61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62b8ea04-3c6e-4326-b568-9cfa4538ef58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01042e44-bb33-4963-a0b8-f498a83a5e61",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "fb5e5e71-18b2-4825-8a85-12c45e91bcc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "fc345c75-8e61-4a51-bd37-9368ba48602a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb5e5e71-18b2-4825-8a85-12c45e91bcc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0aae7400-268a-4bd1-9e92-8f85f0a5ac14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb5e5e71-18b2-4825-8a85-12c45e91bcc5",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "8edf8783-7494-4748-a2af-399780d4c35b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "13e72572-573b-49c6-a4bb-8c0f1d4541b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8edf8783-7494-4748-a2af-399780d4c35b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d982a8-608a-4dd7-9c71-d96cbf05357f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8edf8783-7494-4748-a2af-399780d4c35b",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "547ffb4e-f5e8-41fa-864a-1bbaf7c0191d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "a2ae24f5-2ec2-4a9d-91f7-0a48065998f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "547ffb4e-f5e8-41fa-864a-1bbaf7c0191d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5565091-6ada-4ca0-9990-5fb86fda20ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "547ffb4e-f5e8-41fa-864a-1bbaf7c0191d",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "2e08958a-8dc8-4119-a569-64e9cf95e519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "73e12acf-505f-4088-bf1e-f427c729951f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e08958a-8dc8-4119-a569-64e9cf95e519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7d379e2-8341-4862-bbb5-8123c9e98de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e08958a-8dc8-4119-a569-64e9cf95e519",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "0142fbfb-1bd3-4e05-95cb-5871dcde440e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "432aceb3-4d6d-44bd-a3e4-f2590baae7a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0142fbfb-1bd3-4e05-95cb-5871dcde440e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fc5fce1-c786-432a-aff4-3c901d15c222",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0142fbfb-1bd3-4e05-95cb-5871dcde440e",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "df9f832d-252e-4f77-acf2-b1dfe9efd2a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "08da492d-a8bc-449a-9df3-b901fd7136d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df9f832d-252e-4f77-acf2-b1dfe9efd2a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "517e17cd-8c76-4d8b-a840-485cb04b387c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df9f832d-252e-4f77-acf2-b1dfe9efd2a9",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "8994ff24-9145-48cb-ae14-a2dd07093de7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "3d63e57b-6f02-41b1-ba33-460fec639909",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8994ff24-9145-48cb-ae14-a2dd07093de7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27342e1c-d557-4d9e-b9ba-90908600a921",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8994ff24-9145-48cb-ae14-a2dd07093de7",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "fcebb3bf-71f9-46c2-a589-d7c56e5f9b84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "d0be89eb-b5ca-4b36-a31a-a81f50d5b7ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcebb3bf-71f9-46c2-a589-d7c56e5f9b84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7559793d-539b-41bc-999b-64463987ec28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcebb3bf-71f9-46c2-a589-d7c56e5f9b84",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "f18367e4-3810-4b01-b51b-17d0bbb01a39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "c5088074-d894-426c-b024-5107029f530e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f18367e4-3810-4b01-b51b-17d0bbb01a39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeb9c17f-3c3e-4fdd-ac7d-117c1dfba441",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f18367e4-3810-4b01-b51b-17d0bbb01a39",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "e8ca9f0f-1a5c-4b55-8d97-2bc0980ab64b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "af66a89e-ede4-4d24-989e-0d356d21f416",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8ca9f0f-1a5c-4b55-8d97-2bc0980ab64b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0048c62a-d2d1-4a92-b57b-2deb04de881a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8ca9f0f-1a5c-4b55-8d97-2bc0980ab64b",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "b7019d66-d611-4dd6-b943-639bfb8cea8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "d5d73ccc-86aa-418e-b160-41cc7d607609",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7019d66-d611-4dd6-b943-639bfb8cea8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5e5bde9-0033-4aaf-860c-b4d2e1623589",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7019d66-d611-4dd6-b943-639bfb8cea8f",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "00edfd3c-3d23-4132-9c8f-ad86b0d5f5db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "09b49183-d650-401d-a3e9-933e808cf5fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00edfd3c-3d23-4132-9c8f-ad86b0d5f5db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1c34148-7ab3-464c-9670-201d94415655",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00edfd3c-3d23-4132-9c8f-ad86b0d5f5db",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "7994bcf9-c25f-4756-8fe1-494978c24fc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "525b3950-58b8-4312-bd31-67f011fd83dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7994bcf9-c25f-4756-8fe1-494978c24fc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c3c12de-1654-407e-be08-47ed6e51bf9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7994bcf9-c25f-4756-8fe1-494978c24fc7",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "4d63cda1-2b2a-4e77-94d2-53959c74ffd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "5b367382-952b-4484-9af4-86937c22aa3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d63cda1-2b2a-4e77-94d2-53959c74ffd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "148d89b1-b650-490e-b23b-6cd57ab5f959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d63cda1-2b2a-4e77-94d2-53959c74ffd2",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "517a465c-6750-4ddb-b2e5-f5188f940d10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "8606c0a5-5ada-43f3-b466-fffd86ef3a27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "517a465c-6750-4ddb-b2e5-f5188f940d10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "573ce465-25d6-441a-90d9-f5c81421bf98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "517a465c-6750-4ddb-b2e5-f5188f940d10",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "35bdfc3c-a096-4b0e-93d4-fc20ec1223b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "8716d3a7-d9a6-4fbb-9687-066b1b25fb44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35bdfc3c-a096-4b0e-93d4-fc20ec1223b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20b7111-ec58-4a5f-a757-3897da268004",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35bdfc3c-a096-4b0e-93d4-fc20ec1223b5",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "6adc21ed-c447-4849-a755-8e9bbed50f57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "e8281807-d10d-41e9-81af-12f42d0b6bcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6adc21ed-c447-4849-a755-8e9bbed50f57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b28f0185-bed9-4e2c-8bed-4b323dd23c7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6adc21ed-c447-4849-a755-8e9bbed50f57",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "2314fabb-a0b9-4843-80c5-f5b16328705e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "eadda076-5a31-4f14-9d59-6b350271e1df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2314fabb-a0b9-4843-80c5-f5b16328705e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67b89216-835e-46e0-bff1-c12edc615524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2314fabb-a0b9-4843-80c5-f5b16328705e",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "fde7ab8f-70fe-4c18-8b05-c3f0359de280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "7b9b63c8-8a36-4b2f-86b0-4dd7935623c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde7ab8f-70fe-4c18-8b05-c3f0359de280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "518b7d4d-883e-4c8d-b9f6-75c7a4cec74c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde7ab8f-70fe-4c18-8b05-c3f0359de280",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "d33075ba-17ca-42c3-b2ab-d6f3361dc18b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "7690674f-c235-4da0-a03c-21d41369c028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d33075ba-17ca-42c3-b2ab-d6f3361dc18b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9270e3ae-81c0-4967-bfd6-a1d04257b150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d33075ba-17ca-42c3-b2ab-d6f3361dc18b",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "a939844f-8ce1-4b1e-9187-f0742219a7fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "b8d0562b-9c56-44e5-b3f5-d1bec47a4f18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a939844f-8ce1-4b1e-9187-f0742219a7fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cc2f1a0-d846-4ba0-bc79-8940d582ac2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a939844f-8ce1-4b1e-9187-f0742219a7fe",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "4e238038-8910-4791-ab67-74f0b18a3748",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "710de582-6539-4643-a817-9ff18ba9a80a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e238038-8910-4791-ab67-74f0b18a3748",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30eaa810-42b8-4552-a37c-15eb28712eac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e238038-8910-4791-ab67-74f0b18a3748",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "ba87a971-c862-4718-9c63-ef51a8371d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "6376a843-8401-4245-b9da-e8fc9cf66372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba87a971-c862-4718-9c63-ef51a8371d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eafe94fa-c221-4503-875a-da6cd783b5e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba87a971-c862-4718-9c63-ef51a8371d78",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "b6203772-e74d-4cfa-adbb-d554d5b77a17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "c155eff0-cac9-4318-9037-c0d60a2c077b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6203772-e74d-4cfa-adbb-d554d5b77a17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd1b7cf4-06fb-4e44-ac1d-b6ec05564588",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6203772-e74d-4cfa-adbb-d554d5b77a17",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "3b890b4c-2ff6-4265-b236-d08cbbe97c73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "e88ce68f-7f16-4b2a-ba33-db6c60e30e97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b890b4c-2ff6-4265-b236-d08cbbe97c73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef87dfa4-5d36-4d8c-b8a5-f37828d8eff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b890b4c-2ff6-4265-b236-d08cbbe97c73",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "6c0b9f3f-e14a-4b3d-b718-39b211da3ae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "15abe65a-1804-475a-a92e-1f038dafd530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c0b9f3f-e14a-4b3d-b718-39b211da3ae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf1e6df3-565e-4142-91c7-c3f20768b70c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c0b9f3f-e14a-4b3d-b718-39b211da3ae4",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "7b42b826-6f41-4c45-bdc1-2ebbde535287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "43375f39-e926-4dd3-9032-e422ec0fd307",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b42b826-6f41-4c45-bdc1-2ebbde535287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19e45cd3-4a06-4928-b72b-37c9e1c56861",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b42b826-6f41-4c45-bdc1-2ebbde535287",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "a4eeb94b-df4f-4bb7-95ea-8c8b01cfa902",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "0d3e0a87-92e0-4a20-a01c-d222c64bf72c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4eeb94b-df4f-4bb7-95ea-8c8b01cfa902",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3d05ac4-021a-4a34-ae90-7195e37ec30f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4eeb94b-df4f-4bb7-95ea-8c8b01cfa902",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "f1d0fca4-65b8-4657-be1b-ffc96f231303",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "a0e705e2-9b55-4512-ac9e-4d21b18e90e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d0fca4-65b8-4657-be1b-ffc96f231303",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6a2be8c-04b1-4f59-97ec-c42c5a1d2b4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d0fca4-65b8-4657-be1b-ffc96f231303",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "3cb09f9d-4cb2-464c-82cd-ef45b95cd556",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "737abe92-0aa1-4780-98ea-28f675886f26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cb09f9d-4cb2-464c-82cd-ef45b95cd556",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "059cac85-541c-4186-ae5c-17937ad49fae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cb09f9d-4cb2-464c-82cd-ef45b95cd556",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "f069bd29-d83a-4674-b1e8-95f07ce882ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "5430fa41-7ff1-4e2b-93a4-6e91d28c9786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f069bd29-d83a-4674-b1e8-95f07ce882ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c884671-cc45-422a-9014-df0821bfcba0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f069bd29-d83a-4674-b1e8-95f07ce882ac",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "42018a7a-6019-47d5-a825-dab56fbfef86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "0a49ccc0-1456-411a-8390-6dcee48ff293",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42018a7a-6019-47d5-a825-dab56fbfef86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1824345d-884c-4dba-8bde-be4f004b084b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42018a7a-6019-47d5-a825-dab56fbfef86",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "3feac0a9-b884-4a96-931d-b860b05d2763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "873667da-eed4-43a3-8796-54b9fcc1b7e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3feac0a9-b884-4a96-931d-b860b05d2763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4891c825-9449-4245-9730-625938c67be4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3feac0a9-b884-4a96-931d-b860b05d2763",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "dd629e58-405e-417d-abaa-ddcb190b5a02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "1b7a153f-0cfc-4be3-a66a-650cbda62451",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd629e58-405e-417d-abaa-ddcb190b5a02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0df0af57-8c08-4f05-b57c-f62d5e9bd31c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd629e58-405e-417d-abaa-ddcb190b5a02",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "5c4b6217-0099-4d09-bec6-9fa335f36d1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "ddf09269-91ff-470a-bc28-8f13a2df7576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c4b6217-0099-4d09-bec6-9fa335f36d1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2a2c6b6-90c1-4c5e-ab71-28a033af6b24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c4b6217-0099-4d09-bec6-9fa335f36d1c",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "ece36496-c0b3-4aab-bfc9-1bfcb99a3a4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "e3eff8b9-acf0-456d-a730-4542e4a30904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ece36496-c0b3-4aab-bfc9-1bfcb99a3a4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b435cc04-a64a-4b52-b92a-9c036d7cea51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ece36496-c0b3-4aab-bfc9-1bfcb99a3a4b",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "fd7720e9-5d9c-474f-9c91-7e1b69422736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "f1d8a708-7a37-49f4-b412-2a59ce2cc8c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd7720e9-5d9c-474f-9c91-7e1b69422736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31d36647-19dd-4e23-9776-60d9fe0e4620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd7720e9-5d9c-474f-9c91-7e1b69422736",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "55ef31e7-2e35-4ad0-95d9-0f1ab23f5f73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "632e5e03-3ab5-4646-9a01-bdca85617b1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55ef31e7-2e35-4ad0-95d9-0f1ab23f5f73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4985f5b3-153e-45f9-9c35-3c632569f680",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ef31e7-2e35-4ad0-95d9-0f1ab23f5f73",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "ceddc6c3-8bd4-49ea-9eae-535041dedba8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "92e7dc76-ff7d-400e-815f-719d3306bb68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceddc6c3-8bd4-49ea-9eae-535041dedba8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39994e44-5b31-488d-9dcf-bcfb826495b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceddc6c3-8bd4-49ea-9eae-535041dedba8",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "e9b894ce-d8cc-41c6-aa0d-a8b411a979e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "6e8f7556-7456-49c1-b55b-d3d5bf47d23f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9b894ce-d8cc-41c6-aa0d-a8b411a979e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a38e0f91-7694-4864-8e34-3097f4ec21ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b894ce-d8cc-41c6-aa0d-a8b411a979e6",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "51a2a63c-ce68-4964-bf64-1b33e27fbf94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "d537a3e6-937b-4b58-8c07-9d545b330546",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51a2a63c-ce68-4964-bf64-1b33e27fbf94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f0ba454-0416-4abd-a552-4ca305ac3de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a2a63c-ce68-4964-bf64-1b33e27fbf94",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "19e8ef52-d25f-483b-bbba-b1ef28095331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "6c9e2c67-112c-4f64-98c0-3fdede6f0765",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19e8ef52-d25f-483b-bbba-b1ef28095331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c5758a6-3689-4a40-b726-5ebf539dc925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19e8ef52-d25f-483b-bbba-b1ef28095331",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "73be02f5-7f63-45ea-9be6-bcc0a899ee6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "7635abaa-67e7-4c44-be54-2871740f2c64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73be02f5-7f63-45ea-9be6-bcc0a899ee6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9b0a81a-2d7a-4289-a8ea-c273d681fb36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73be02f5-7f63-45ea-9be6-bcc0a899ee6d",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "432c676e-5624-4ef7-8c44-2f6dc48201e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "cd6b03b2-9f69-4bb6-bed0-ce98fcfb9ed6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "432c676e-5624-4ef7-8c44-2f6dc48201e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6249f5f-5161-4b65-8103-bad4475c7f6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "432c676e-5624-4ef7-8c44-2f6dc48201e6",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "b068bb52-d93c-4cb9-b4ee-db40d0545ae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "291ada54-37a2-468d-b89c-e6f59e940887",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b068bb52-d93c-4cb9-b4ee-db40d0545ae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ad34756-549e-408e-8efc-602026988d3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b068bb52-d93c-4cb9-b4ee-db40d0545ae3",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "4182d477-6448-457f-b32e-a4123ccc8075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "ee9beb37-b7e5-4f7a-a7f5-3bbaa2a03e15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4182d477-6448-457f-b32e-a4123ccc8075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "582321f1-e447-4787-9a94-9a98cda8158e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4182d477-6448-457f-b32e-a4123ccc8075",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "5ec4bde8-94ad-4f6b-a032-83b33ca90526",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "045a44e2-04ef-4a4d-9ec8-f55504c0ffdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ec4bde8-94ad-4f6b-a032-83b33ca90526",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b982b1c1-f18a-45ca-ab1f-27f5497b44ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ec4bde8-94ad-4f6b-a032-83b33ca90526",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "2510b06f-9cb0-492a-8441-c152eb9b039e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "92f2d6b1-c8fd-45d3-b050-6a98ac40b319",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2510b06f-9cb0-492a-8441-c152eb9b039e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aafbd6e-efb7-4482-b8af-dec2174ef0fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2510b06f-9cb0-492a-8441-c152eb9b039e",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "2cc73a5a-593b-41ce-b13a-120292df5ea1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "ad5dab9e-3463-42ea-8173-9140ea430b90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cc73a5a-593b-41ce-b13a-120292df5ea1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8403ca5-362c-419e-aa00-adfa47224d9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cc73a5a-593b-41ce-b13a-120292df5ea1",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "c8255fed-ff25-4152-8ca8-ab7fe2c0ccdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "401779fd-afab-41e7-95a9-833f0cb7d4bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8255fed-ff25-4152-8ca8-ab7fe2c0ccdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "098d8e5c-2d5a-452f-bc43-a131fc036f42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8255fed-ff25-4152-8ca8-ab7fe2c0ccdf",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "0a66d72c-173f-4acd-b666-53a664a017bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "9c0593d8-249a-463c-bb99-9567d646ea7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a66d72c-173f-4acd-b666-53a664a017bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3d32963-06fb-4ad5-b266-c5fba7c9bf80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a66d72c-173f-4acd-b666-53a664a017bb",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "4f759a57-57b2-4938-9481-78f030714121",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "717fb36c-6847-4e3e-a6e3-48436c6702ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f759a57-57b2-4938-9481-78f030714121",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d469e84c-cb0f-4ccd-8827-06873e3f0091",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f759a57-57b2-4938-9481-78f030714121",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "5dc782d7-9a5f-487d-b64f-afce34622076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "6a2d6fc4-fe8c-4045-8872-eb4ac32263c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dc782d7-9a5f-487d-b64f-afce34622076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cc4b0da-a103-4331-864d-756932c009c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dc782d7-9a5f-487d-b64f-afce34622076",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "ff97fd60-8ada-4c3a-b330-ecf84676ff8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "e6f632de-014a-4c4a-8135-651662a8c613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff97fd60-8ada-4c3a-b330-ecf84676ff8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c2c25ca-cba9-4482-b6ce-151e9dda7b65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff97fd60-8ada-4c3a-b330-ecf84676ff8c",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "b839599e-4007-42fb-bd2c-32cb365d97e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "3573362b-8f8f-4253-a1b5-f5a742b93fd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b839599e-4007-42fb-bd2c-32cb365d97e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94600028-7771-454a-88f1-522eca2a55f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b839599e-4007-42fb-bd2c-32cb365d97e7",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "e2d53511-f808-42c2-8a1e-bc68d1fc4102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "4df534ba-2e35-48f1-8e56-378643dc95e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2d53511-f808-42c2-8a1e-bc68d1fc4102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60f92145-f847-4fa8-ae6b-df27c2c80f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2d53511-f808-42c2-8a1e-bc68d1fc4102",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "453bd361-b7b7-4797-9ac1-5470539cd6f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "81f96137-f043-4ea5-8fa4-3dc554a90f07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "453bd361-b7b7-4797-9ac1-5470539cd6f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f41a5ad7-8d18-46cb-8c0f-7aaf79d7d6ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "453bd361-b7b7-4797-9ac1-5470539cd6f7",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "dc8a6849-77d5-482d-93e8-9bfe4b447885",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "b5f8677c-a34b-4cd8-ae26-e4db49f8e2c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc8a6849-77d5-482d-93e8-9bfe4b447885",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ef13aff-ccd4-45db-8f03-967fbd008bf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc8a6849-77d5-482d-93e8-9bfe4b447885",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "cd081ed6-5340-4d5b-8372-81f0d4b31e5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "53dc6981-7b15-49cf-b40a-293d4d7f7670",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd081ed6-5340-4d5b-8372-81f0d4b31e5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a53fca-ecdc-4eb6-95d8-07c328f1f44b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd081ed6-5340-4d5b-8372-81f0d4b31e5e",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "09505f8e-6939-4ee7-9033-c1122feab96e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "c08009e2-13fd-4dc2-ac19-370adadfa42e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09505f8e-6939-4ee7-9033-c1122feab96e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcf215e7-4a61-4b9a-bb01-3736856e49a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09505f8e-6939-4ee7-9033-c1122feab96e",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "0f4ea8f7-36d5-4449-b727-756cb08b789e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "33c0bb72-91c2-487f-9a0f-8ed8428ed157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f4ea8f7-36d5-4449-b727-756cb08b789e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f53239b7-5a89-49d5-8f7d-7586409dc042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f4ea8f7-36d5-4449-b727-756cb08b789e",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "bad02a7b-51b8-4f06-a9d7-8cda0d29c93e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "256d922c-2606-45f6-b7e0-c31b8c79d077",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bad02a7b-51b8-4f06-a9d7-8cda0d29c93e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7170564c-1867-420f-92bf-3f0915945a7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bad02a7b-51b8-4f06-a9d7-8cda0d29c93e",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "cd2ae142-0f14-4b47-991e-c61c272324c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "2c85b2ec-311c-4252-a074-85195cf08dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd2ae142-0f14-4b47-991e-c61c272324c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63e0be7f-57a4-494c-a346-b609ad8ad894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd2ae142-0f14-4b47-991e-c61c272324c9",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "7e1c2ab6-ab55-482b-b97b-f4672f1bac14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "cc422094-21ef-4fc9-9403-dd09671facb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e1c2ab6-ab55-482b-b97b-f4672f1bac14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd856540-5b4e-47c5-ad4e-c36d78fd3648",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e1c2ab6-ab55-482b-b97b-f4672f1bac14",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "765a83c7-1e0e-460a-9d72-426c810ca2fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "44727cc1-9a86-4e39-b272-8528de3ff888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "765a83c7-1e0e-460a-9d72-426c810ca2fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e602f65b-5a1d-4330-8467-4461acc2ebe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "765a83c7-1e0e-460a-9d72-426c810ca2fc",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "0b3502fc-d878-4983-92a4-028818544c36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "80a0ea07-b410-4e99-b109-b2316f87ec52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b3502fc-d878-4983-92a4-028818544c36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d47321a0-42fa-4fab-a97d-1ad460433f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b3502fc-d878-4983-92a4-028818544c36",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "527bc993-3758-4b16-a5f7-9571b106c4ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "a36960fb-f017-42a2-bad6-7ecfbea0c621",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "527bc993-3758-4b16-a5f7-9571b106c4ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0e4604f-ead0-4bd3-b4cc-d76ac7bec954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "527bc993-3758-4b16-a5f7-9571b106c4ba",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        },
        {
            "id": "8497c4bb-654e-411a-9b47-3bfa27bab339",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "compositeImage": {
                "id": "692d24fd-6a60-487b-82c0-0ce3216ba900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8497c4bb-654e-411a-9b47-3bfa27bab339",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c7b78a5-658c-4b1d-970e-245ce05ccbe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8497c4bb-654e-411a-9b47-3bfa27bab339",
                    "LayerId": "6f835898-0fba-4c12-b75f-0a4860bc0e00"
                }
            ]
        }
    ],
    "height": 8,
    "layers": [
        {
            "id": "6f835898-0fba-4c12-b75f-0a4860bc0e00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "788f056d-5eac-4098-abdf-b7ae5ca0dc13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}
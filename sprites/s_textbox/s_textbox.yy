{
    "id": "37551299-ea7e-41a5-80a3-f5c1c9f71a42",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c7678b7c-8019-4b1a-937e-facd175ff507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37551299-ea7e-41a5-80a3-f5c1c9f71a42",
            "compositeImage": {
                "id": "e77840b9-1c7b-4b3f-a976-b4e388664559",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7678b7c-8019-4b1a-937e-facd175ff507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccdd6168-cbb3-4e1c-ba9a-a4ecb1589368",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7678b7c-8019-4b1a-937e-facd175ff507",
                    "LayerId": "963c0649-254c-4af2-81f1-ea6d5f2a6178"
                }
            ]
        }
    ],
    "height": 12,
    "layers": [
        {
            "id": "963c0649-254c-4af2-81f1-ea6d5f2a6178",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37551299-ea7e-41a5-80a3-f5c1c9f71a42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}
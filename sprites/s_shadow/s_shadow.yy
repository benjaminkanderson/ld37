{
    "id": "95ad4a0a-bce8-4047-9842-ff7a1fb1eda2",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0f767de2-3f07-42ff-8515-70306ddb8bfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ad4a0a-bce8-4047-9842-ff7a1fb1eda2",
            "compositeImage": {
                "id": "df9b4e08-96a8-422e-af00-1156207e64d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f767de2-3f07-42ff-8515-70306ddb8bfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8a17f8d-7c0d-4b3e-ad71-4228a0ccc89d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f767de2-3f07-42ff-8515-70306ddb8bfb",
                    "LayerId": "5c8ba736-7185-46c1-badc-687e3627ab6c"
                }
            ]
        }
    ],
    "height": 16,
    "layers": [
        {
            "id": "5c8ba736-7185-46c1-badc-687e3627ab6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95ad4a0a-bce8-4047-9842-ff7a1fb1eda2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
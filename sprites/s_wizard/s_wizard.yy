{
    "id": "4e13926c-3d41-45c3-9e90-858280a857b7",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_wizard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "37d9a85e-4349-41c0-8814-ab429a7376a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e13926c-3d41-45c3-9e90-858280a857b7",
            "compositeImage": {
                "id": "07e3ebb2-2a4a-4194-9779-8dab424028e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37d9a85e-4349-41c0-8814-ab429a7376a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce5d8ba2-ca3a-4a52-bdc6-75f8a5586b6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37d9a85e-4349-41c0-8814-ab429a7376a1",
                    "LayerId": "057171d3-f03e-4646-a418-6f5a211d02cb"
                }
            ]
        },
        {
            "id": "cf639e79-810f-4e0f-8592-7e801c51fcc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e13926c-3d41-45c3-9e90-858280a857b7",
            "compositeImage": {
                "id": "836d29aa-e73a-4d12-a388-c8edeaf32f5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf639e79-810f-4e0f-8592-7e801c51fcc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dd6522a-99f7-4faf-a040-f97b9005097c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf639e79-810f-4e0f-8592-7e801c51fcc8",
                    "LayerId": "057171d3-f03e-4646-a418-6f5a211d02cb"
                }
            ]
        },
        {
            "id": "3c1ad198-6fad-44df-98c5-4919ae49b668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e13926c-3d41-45c3-9e90-858280a857b7",
            "compositeImage": {
                "id": "eb90bc87-914d-4fc1-909a-3d68640ff090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c1ad198-6fad-44df-98c5-4919ae49b668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5899018-9934-42ae-bfbc-bf650dba8d08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c1ad198-6fad-44df-98c5-4919ae49b668",
                    "LayerId": "057171d3-f03e-4646-a418-6f5a211d02cb"
                }
            ]
        },
        {
            "id": "3d627bfb-28ac-417a-bdbf-d8f998c62f25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e13926c-3d41-45c3-9e90-858280a857b7",
            "compositeImage": {
                "id": "4962a706-1735-44a0-afdf-dd5ee6789062",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d627bfb-28ac-417a-bdbf-d8f998c62f25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af082343-742e-4957-8930-f3cb488dadb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d627bfb-28ac-417a-bdbf-d8f998c62f25",
                    "LayerId": "057171d3-f03e-4646-a418-6f5a211d02cb"
                }
            ]
        }
    ],
    "height": 16,
    "layers": [
        {
            "id": "057171d3-f03e-4646-a418-6f5a211d02cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e13926c-3d41-45c3-9e90-858280a857b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 4
}
{
    "id": "3573793c-da73-438b-85a2-7aa0e4c245dd",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_move_action_cue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5dc328b4-a140-4f47-8091-bf20e221bc03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3573793c-da73-438b-85a2-7aa0e4c245dd",
            "compositeImage": {
                "id": "cd8d0f67-b7bf-43c3-822d-671dbf9e5649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dc328b4-a140-4f47-8091-bf20e221bc03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e36884a5-bdec-4eb6-8b00-dc8ea5a0e562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dc328b4-a140-4f47-8091-bf20e221bc03",
                    "LayerId": "6db74e36-98a9-40a7-985b-a735336c9b7a"
                }
            ]
        }
    ],
    "height": 16,
    "layers": [
        {
            "id": "6db74e36-98a9-40a7-985b-a735336c9b7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3573793c-da73-438b-85a2-7aa0e4c245dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
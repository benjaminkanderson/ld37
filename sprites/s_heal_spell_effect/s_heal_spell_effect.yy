{
    "id": "baac7a45-9400-42ee-92fc-dac0aeeb642d",
    "modelName": "GMSprite",
    "mvc": "1.1",
    "name": "s_heal_spell_effect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a6c5c480-b60e-4f93-b5b0-63255e0f55cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baac7a45-9400-42ee-92fc-dac0aeeb642d",
            "compositeImage": {
                "id": "6673aec1-09f0-4cf7-bdcd-4c3d819cf823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6c5c480-b60e-4f93-b5b0-63255e0f55cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3659fdae-275a-4838-a6e8-1c1433300063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6c5c480-b60e-4f93-b5b0-63255e0f55cd",
                    "LayerId": "bc369406-8b03-4b0a-a441-64e3ffcaa200"
                }
            ]
        },
        {
            "id": "32265375-1463-4da1-bfdd-5483d9c23b9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baac7a45-9400-42ee-92fc-dac0aeeb642d",
            "compositeImage": {
                "id": "88b8186f-d175-4a82-8ee2-fb9b7a435489",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32265375-1463-4da1-bfdd-5483d9c23b9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a945cb9c-de3b-4024-9efc-83487d1531c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32265375-1463-4da1-bfdd-5483d9c23b9d",
                    "LayerId": "bc369406-8b03-4b0a-a441-64e3ffcaa200"
                }
            ]
        }
    ],
    "height": 12,
    "layers": [
        {
            "id": "bc369406-8b03-4b0a-a441-64e3ffcaa200",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "baac7a45-9400-42ee-92fc-dac0aeeb642d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}